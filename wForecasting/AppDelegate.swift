//
//  AppDelegate.swift
//  wForecasting
//
//  Created by Dilip Saket on 17/09/21.
//

import UIKit
import CoreLocation
import Firebase

//@main
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

    var window: UIWindow?
    var rootViewController:ViewController!
    var navController:UINavigationController!

    var locationManager:CLLocationManager!
    var strLocality = "Current location"
    var strCountryCode = ""
    var mUserLocation:CLLocation!
    
    var ref: DatabaseReference!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        ref = Database.database().reference()
        
        self.rootViewController = ViewController()
        self.navController = UINavigationController.init(rootViewController: self.rootViewController)
        self.navController.isNavigationBarHidden = true

        self.window = UIWindow.init()
        self.window?.bounds = UIScreen.main.bounds
        self.window?.rootViewController = self.navController
        self.window?.backgroundColor = .white
        self.window?.makeKeyAndVisible()

        self.determineCurrentLocation()

        return true
    }
    
    //MARK:- Locations
    func determineCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }

    //MARK:- CLLocationManagerDelegate Methods
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error - locationManager: \(error.localizedDescription)")
    }
    //MARK:- Intance Methods

    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        mUserLocation = locations[0] as CLLocation
        self.strLocality = self.setUsersClosestLocation(mLattitude: mUserLocation.coordinate.latitude, mLongitude: mUserLocation.coordinate.longitude)
    }
    //MARK:- Intance Methods

    func setUsersClosestLocation(mLattitude: CLLocationDegrees, mLongitude: CLLocationDegrees) -> String {
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: mLattitude, longitude: mLongitude)

        geoCoder.reverseGeocodeLocation(location) {
            (placemarks, error) -> Void in

            if let mPlacemark = placemarks{
                print("mPlacemark[0] = ", mPlacemark[0])
                self.strLocality = mPlacemark[0].locality!
                self.strCountryCode = mPlacemark[0].isoCountryCode!
                
                UserDefaults.standard.setValue(self.strLocality, forKey: "strLocality")
                UserDefaults.standard.setValue(self.strCountryCode, forKey: "strCountryCode")
            }
        }
        return strLocality
    }

}
