//
//  GlobalVariables.swift
//  wForecasting
//
//  Created by Dilip Saket on 18/09/21.
//

import Foundation
import UIKit

let screenWidth:CGFloat = UIScreen.main.bounds.width
let screenHeight:CGFloat = UIScreen.main.bounds.height
let tabBarHeight:CGFloat = 100

let strBaseURL = "https://community-open-weather-map.p.rapidapi.com/"

let blueTheme:UIColor = UIColor.init(red: 171/255.0, green: 207/255.0, blue: 226/255.0, alpha: 1.0)
