//
//  ViewController.swift
//  wForecasting
//
//  Created by Dilip Saket on 17/09/21.
//

import UIKit

class ViewController: UIViewController {

    let viewTabBar:UIView = UIView()
    let btnToday:UIButton = UIButton()
    let btnForeCast:UIButton = UIButton()
        
    let todayVC:TodayViewController = TodayViewController()
    let forecastVC:ForecastViewController = ForecastViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        self.title = "Today"
        
        self.designTabBar()
        self.designTodayAndForecastVC()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)                
    }
    //MARK:- Tabbar
    func designTabBar() {
        
        //Ceating Bottom Custom UI same as tab bar
        self.viewTabBar.frame = CGRect(x: -1, y: screenHeight-100, width: screenWidth+2, height: tabBarHeight+1)
        self.viewTabBar.backgroundColor = .white
        self.viewTabBar.layer.borderWidth = 1.0
        self.viewTabBar.layer.borderColor = UIColor.gray.cgColor
        self.view.addSubview(self.viewTabBar)
        
        //Adding Today And Forecast Button on tab ui
        self.btnToday.frame = CGRect(x: 1, y: 0, width: screenWidth/2.0, height: viewTabBar.frame.size.height)
        self.btnToday.backgroundColor = .clear
        self.btnToday.setTitle("Today", for: .normal)
        self.btnToday.setImage(UIImage.init(named: "today"), for: .normal)
        self.btnToday.setImage(UIImage.init(named: "today")!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        self.btnToday.tintColor = UIColor.systemBlue
        self.btnToday.setTitleColor(UIColor.systemBlue, for: .normal)
        self.btnToday.setTitleColor(UIColor.systemBlue, for: .selected)
        self.btnToday.setTitleColor(UIColor.systemBlue, for: .highlighted)
        self.viewTabBar.addSubview(self.btnToday)
        
        self.btnForeCast.frame = CGRect(x: 1+(screenWidth/2.0), y: 0, width: screenWidth/2.0, height: viewTabBar.frame.size.height)
        self.btnForeCast.backgroundColor = .clear
        self.btnForeCast.setTitle("Forecast", for: .normal)
        self.btnForeCast.setImage(UIImage.init(named: "forecast"), for: .normal)
        self.btnForeCast.setImage(UIImage.init(named: "forecast")!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        self.btnForeCast.tintColor = UIColor.darkGray
        self.btnForeCast.setTitleColor(UIColor.darkGray, for: .normal)
        self.btnForeCast.setTitleColor(UIColor.darkGray, for: .selected)
        self.btnForeCast.setTitleColor(UIColor.darkGray, for: .highlighted)
        self.viewTabBar.addSubview(self.btnForeCast)
        
        //Center align the image and text of button
        self.adjustImageAndTitleOffsetsForButton(button: self.btnToday)
        self.adjustImageAndTitleOffsetsForButton(button: self.btnForeCast)
       
        //Adding same actions on booth the button
        self.btnToday.addTarget(self, action: #selector(btnTabClicked(_ :)), for: .touchDown)
        self.btnForeCast.addTarget(self, action: #selector(btnTabClicked(_ :)), for: .touchDown)
    }
 
    @objc func btnTabClicked(_ sender:UIButton) {
        
        self.todayVC.view.isHidden = true
        self.forecastVC.view.isHidden = true
        if(sender == self.btnToday) {
            self.todayVC.view.isHidden = false
            self.todayVC.refreshWeatherData()
            
            self.btnToday.tintColor = UIColor.systemBlue
            self.btnToday.setTitleColor(UIColor.systemBlue, for: .normal)
            self.btnToday.setTitleColor(UIColor.systemBlue, for: .selected)
            self.btnToday.setTitleColor(UIColor.systemBlue, for: .highlighted)
            
            self.btnForeCast.tintColor = UIColor.darkGray
            self.btnForeCast.setTitleColor(UIColor.darkGray, for: .normal)
            self.btnForeCast.setTitleColor(UIColor.darkGray, for: .selected)
            self.btnForeCast.setTitleColor(UIColor.darkGray, for: .highlighted)
            
        } else {
            self.forecastVC.view.isHidden = false
            self.forecastVC.refreshWeatherData()
            
            self.btnToday.tintColor = UIColor.darkGray
            self.btnToday.setTitleColor(UIColor.darkGray, for: .normal)
            self.btnToday.setTitleColor(UIColor.darkGray, for: .selected)
            self.btnToday.setTitleColor(UIColor.darkGray, for: .highlighted)
            
            self.btnForeCast.tintColor = UIColor.systemBlue
            self.btnForeCast.setTitleColor(UIColor.systemBlue, for: .normal)
            self.btnForeCast.setTitleColor(UIColor.systemBlue, for: .selected)
            self.btnForeCast.setTitleColor(UIColor.systemBlue, for: .highlighted)
        }
    }
    
    private func adjustImageAndTitleOffsetsForButton (button: UIButton) {

        let spacing: CGFloat = 6.0
        let imageSize = button.imageView!.frame.size
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: -imageSize.width, bottom: -(imageSize.height + spacing), right: 0)
        let titleSize = button.titleLabel!.frame.size
        button.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacing), left: 0, bottom: 0, right: -titleSize.width)
   }

    //MARK:- TodayAndForecastVC
    func designTodayAndForecastVC() {
        //Design Today's view and show
        self.todayVC.view.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight-self.viewTabBar.frame.size.height+1)
        self.todayVC.view.backgroundColor = UIColor.clear
        self.view.addSubview(self.todayVC.view)
        
        //Design Forecast's view and show
        self.forecastVC.view.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight-self.viewTabBar.frame.size.height)
        self.forecastVC.view.backgroundColor = UIColor.clear
        self.view.addSubview(self.forecastVC.view)
        self.forecastVC.view.isHidden = true
    }
}

