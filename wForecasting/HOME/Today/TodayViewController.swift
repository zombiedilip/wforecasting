//
//  TodayViewController.swift
//  wForecasting
//
//  Created by Dilip Saket on 17/09/21.
//

import UIKit
import Firebase
//import FirebaseDatabase
//import FirebaseAuth

class TodayViewController: BaseViewController {

    let viewTopBar = UIView()
    let lblTopBar = UILabel()
    
    let imgViewWeather:UIImageView = UIImageView()
    let lblWeatherLocation:UILabel = UILabel()
    let lblWeatherDetails:UILabel = UILabel()
    
    var dictWeather:[String:Any] = [:]
    let strEmail = UIDevice.init().identifierForVendor!.uuidString+"@gmail.com"
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.designTopBar()
        self.designingWeatherView()
        
        self.perform(#selector(refreshWeatherData), with: nil, afterDelay:2)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.refreshWeatherData()
    }
    
    func designTopBar() {
        
        //Ceating Top Bar UI 
        self.viewTopBar.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 84)
        self.viewTopBar.backgroundColor = .white
        self.viewTopBar.layer.borderWidth = 1.0
        self.viewTopBar.layer.borderColor = UIColor.gray.cgColor
        self.view.addSubview(self.viewTopBar)
                
        self.lblTopBar.frame = CGRect(x: 0, y: 20, width: screenWidth, height: 64)
        self.lblTopBar.backgroundColor = .clear
        self.lblTopBar.text = "Today"
        self.lblTopBar.font = UIFont.init(name: "Helvetica-Bold", size: 32.0)
        self.lblTopBar.textColor = .gray
        self.lblTopBar.textAlignment = .center
        self.view.addSubview(self.lblTopBar)
    }
    
    func designingWeatherView() {
        let logoWH:CGFloat = 100.0
        
        var yRef:CGFloat = (screenHeight/4.0)-100
        
        //Weather Type Logo
        self.imgViewWeather.frame = CGRect(x: (screenWidth-logoWH)/2.0, y: yRef, width: logoWH, height: logoWH)
        self.imgViewWeather.image = UIImage.init(named: "today")
        self.view.addSubview(self.imgViewWeather)
        
        yRef = yRef+self.imgViewWeather.frame.size.height
        
        //Location
        self.lblWeatherLocation.frame = CGRect(x: 0, y: yRef, width: screenWidth, height: 30)
        self.lblWeatherLocation.text = "Location"
        self.lblWeatherLocation.textAlignment = .center
        self.lblWeatherLocation.font = UIFont.init(name: "Helvetica", size: 24.0)
        self.lblWeatherLocation.textColor = .gray
        self.view.addSubview(self.lblWeatherLocation)
        
        yRef = yRef+self.lblWeatherLocation.frame.size.height
        
        //Weather | Details
        self.lblWeatherDetails.frame = CGRect(x: 0, y: yRef, width: screenWidth, height: 70)
        self.lblWeatherDetails.text = "Weather | Details"
        self.lblWeatherDetails.textAlignment = .center
        self.lblWeatherDetails.font = UIFont.init(name: "Helvetica", size: 40.0)
        self.lblWeatherDetails.textColor = blueTheme
        self.view.addSubview(self.lblWeatherDetails)
    }
    
    //MARK:- APIs
    @objc func refreshWeatherData() {
        if BaseViewController.isConnectedToNetwork(){
            self.addLoader()
            self.getTodayWeatherFromAPI()
        } else {
            let dict:NSDictionary? = UserDefaults.standard.value(forKey: "weather") as? NSDictionary
            if(dict != nil) {
                self.parseAndShowResponse(dict!)
            } else {
                self.showAlertMessage(title: "No internet connection!", message: "Please connect to internet to get updated weather details.")
            }
        }
    }
    
    func getTodayWeatherFromAPI() {

        let headers = [
            "x-rapidapi-host": "community-open-weather-map.p.rapidapi.com",
            "x-rapidapi-key": "778da6ad37msh87e4ba9f0f93259p18f1d1jsnfb0fd346cb30"
        ]
        let strURL = strBaseURL+"weather?q="+appDelegate.strLocality.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!

        let request = NSMutableURLRequest(url: NSURL(string: strURL)! as URL,
                                                cachePolicy: .useProtocolCachePolicy,
                                            timeoutInterval: 100.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers

        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            DispatchQueue.main.async {
                self.removeLoader()
                if (error != nil) {
                //    print(error)
                } else {
                    guard data != nil else {
                        print("no data found: \(String(describing: error))")
                                    return
                    }

                    do {
                        if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
//                            print("Response: \(json)")                            
                            self.parseAndShowResponse(json)
                        }
                    } catch let parseError {
                        print(parseError)// Log the error thrown by `JSONObjectWithData`
                        let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        self.showAlertMessage(jsonStr! as String)
                        self.lblWeatherLocation.text = self.appDelegate.strLocality+", "+self.appDelegate.strCountryCode
                    }
                }
            }
        })

        dataTask.resume()
    }
    
    //MARK:- Data parsing and showing
    func parseAndShowResponse(_ result:NSDictionary){
        
        UserDefaults.standard.setValue(result, forKey: "weather")
        
        self.dictWeather = result as! [String:Any]
        let statusCode:Int = dictWeather["cod"] as? Int ?? 0
        if(statusCode == 200) {
            self.displayDataOnScreen(dictWeather)
        } else {
            self.showAlertMessage(title:appDelegate.strLocality, message:dictWeather["message"] as? String ?? "No record found")
        }
    }
    
    func displayDataOnScreen(_ dictItem: [String:Any]?) {
        
        if(dictItem == nil) {
            return
        }
        
        let dictMain:[String:Any]?  = (dictItem?["main"] as? [String:Any])
        if(dictMain == nil) {
            return
        }
        
        let arrWeather:[[String:Any]]  = (dictItem?["weather"] as? [[String:Any]]) ?? [[:]]
        if(arrWeather.count == 0) {
            return
        }
        
        let dictWeather:[String:Any] = arrWeather.first!
        let main:String = dictWeather["main"] as? String ?? ""
        let temp:Double = dictMain!["temp"] as? Double ?? 0
        //FORMULA = 290.97K − 273.15 = 17.82°C
        if(temp > 273.15) {
            self.lblWeatherDetails.text = String(Int(temp-273.15)) + "°C" + " | " + main
        }
        self.lblWeatherLocation.text = appDelegate.strLocality+", "+appDelegate.strCountryCode
        
        let img:UIImage! = UIImage.init(named: main.lowercased()) ?? UIImage.init(named: "today")!
        self.imgViewWeather.image = img
        
        self.saveTemperatureToFirebase()
    }
    
    func saveTemperatureToFirebase()
    {
        if(Auth.auth().currentUser != nil) {
            let intCreationTime:Int = Date().millisecondsSince1970
            let userID:String = (Auth.auth().currentUser?.uid)!
            let strPath:String = "users/" + userID + "/weather/"
            let strKey:String = String(intCreationTime)
            
            let postGame = ["latitude": appDelegate.mUserLocation.coordinate.latitude,
                            "longitude": appDelegate.mUserLocation.coordinate.longitude,
                            "location_detaoils": appDelegate.strLocality+appDelegate.strCountryCode,
                            "temperature": self.lblWeatherDetails.text!] as [String : Any]
            
            appDelegate.ref.child(strPath).child(strKey).setValue(postGame) {
                
                (error, ref) in
                if error != nil {
                    print(error!)
                }
                else {
                    print("Temperature saved successfully!")
                }
            }
        } else {
            self.signInUser()
        }
    }
    
    func createUser() {
        self.addLoader()
        Auth.auth().createUser(withEmail: strEmail, password: "password") { (authResult, error) in
            // ...
            self.removeLoader()
            guard let _ = authResult?.user else {
                print(error.debugDescription)
                self.showAlertMessage((error?.localizedDescription)!)
                return
            }
            self.saveTemperatureToFirebase()
        }
    }
    
    func signInUser() {
        self.addLoader()
        Auth.auth().signIn(withEmail: strEmail, password: "password") { (user, error) in
            
            self.removeLoader()
            if(error == nil){
            } else {
                self.createUser()
            }
        }
    }
}


extension Date {
    
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}
