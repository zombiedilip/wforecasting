//
//  WeatherTableViewCell.swift
//  wForecasting
//
//  Created by Dilip Saket on 18/09/21.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {
            
    override func prepareForReuse() {
        self.textLabel!.text = ""
        self.detailTextLabel!.text = ""
        self.imageView?.image = UIImage.init(named: "today")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.textLabel!.textColor = .gray
        self.detailTextLabel!.textColor = blueTheme
        
        self.textLabel!.numberOfLines = 0
        self.detailTextLabel!.numberOfLines = 0
        
        self.textLabel!.font = UIFont.init(name: "Helvetica", size: 17.0)
        self.detailTextLabel!.font = UIFont.init(name: "Helvetica", size: 32.0)
        self.selectionStyle = .none
        self.backgroundColor = .clear
        self.accessoryType = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setWeatherParameters(_ objWeather: WeatherObject) {        
        self.textLabel!.text = objWeather.strDidplayTime+"\n"+objWeather.strName+"\n"
        self.detailTextLabel!.text = objWeather.strTemp+"°"
        
        let img:UIImage! = UIImage.init(named: objWeather.strName.lowercased()) ?? UIImage.init(named: "today")!
        self.imageView?.image = img
    }
}
