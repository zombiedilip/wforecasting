//
//  ForecastViewController.swift
//  wForecasting
//
//  Created by Dilip Saket on 17/09/21.
//

import UIKit

class ForecastViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    let viewTopBar = UIView()
    let lblTopBar = UILabel()
    
    let tblWeather:UITableView = UITableView()
    
    var arrItems:[[String:Any]] = [[:]]
    var arrWeathers:[WeatherObject] = []
    
    var weatherData:[String:[WeatherObject]] = [:]
    var arrKeys:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.designTopBar()
        self.designingForecastView()
    }
    
    func designTopBar() {
        
        //Ceating Top Bar UI
        self.viewTopBar.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 84)
        self.viewTopBar.backgroundColor = .white
        self.viewTopBar.layer.borderWidth = 1.0
        self.viewTopBar.layer.borderColor = UIColor.gray.cgColor
        self.view.addSubview(self.viewTopBar)
                
        self.lblTopBar.frame = CGRect(x: 0, y: 20, width: screenWidth, height: 64)
        self.lblTopBar.backgroundColor = .clear
        self.lblTopBar.text = "Forecast"
        self.lblTopBar.font = UIFont.init(name: "Helvetica-Bold", size: 32.0)
        self.lblTopBar.textColor = .gray
        self.lblTopBar.textAlignment = .center
        self.view.addSubview(self.lblTopBar)
    }
    
    func designingForecastView() {
        
        //Weather table designing
        tblWeather.frame = CGRect(x: 0, y: self.viewTopBar.frame.height, width: screenWidth, height: screenHeight-self.viewTopBar.frame.height-tabBarHeight)
        tblWeather.delegate = self
        tblWeather.dataSource = self
        tblWeather.separatorStyle = .singleLine
        self.view.addSubview(tblWeather)
    }
    
    //MARK:- UITableView Delegates
   func numberOfSections(in tableView: UITableView) -> Int {
    return self.arrKeys.count
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.weatherData[self.arrKeys[section]]?.count ?? 0
   }
   
   func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
   }
   
   func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewHeader:UIView = UIView()
        viewHeader.frame = CGRect(x:0, y: 0, width: screenWidth, height: 44)
        viewHeader.backgroundColor = UIColor.init(white: 0.9, alpha: 0.9)
        
        let lblTitle = UILabel()
        lblTitle.frame = CGRect(x:20, y: 0, width: screenWidth-40, height: 44)
        lblTitle.text = self.arrKeys[section].uppercased()
        lblTitle.font = UIFont.init(name: "Helvetica", size: 20.0)
        lblTitle.adjustsFontSizeToFitWidth = true
        lblTitle.textColor = .gray
        lblTitle.backgroundColor = .clear
        viewHeader.addSubview(lblTitle)
        return viewHeader
   }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CellIdentifier:String = String(indexPath.row);

        var cell: WeatherTableViewCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? WeatherTableViewCell
        if cell == nil {
            autoreleasepool {
                cell = WeatherTableViewCell(style: .value1, reuseIdentifier: CellIdentifier)
            }
        }

        let arrW:[WeatherObject] = self.weatherData[self.arrKeys[indexPath.section]]!
        cell?.setWeatherParameters(arrW[indexPath.row])
        
        return cell!
    }
    
    //MARK:-
    
    func refreshWeatherData() {
        if BaseViewController.isConnectedToNetwork(){
            self.addLoader()
            self.getWeatherDetailsFromAPI()
        } else {
            let dict:NSDictionary? = UserDefaults.standard.value(forKey: "forecast") as? NSDictionary
            if(dict != nil) {
                self.parseAndShowResponse(dict!)
            } else {
                self.showAlertMessage(title: "No internet connection!", message: "Please connect to internet to get updated weather details.")
            }
        }
    }
    
    //MARK:- APIs
    func getWeatherDetailsFromAPI() {

        let headers = [
            "x-rapidapi-host": "community-open-weather-map.p.rapidapi.com",
            "x-rapidapi-key": "778da6ad37msh87e4ba9f0f93259p18f1d1jsnfb0fd346cb30"
        ]

        let strURL = strBaseURL+"forecast?q="+appDelegate.strLocality.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let request = NSMutableURLRequest(url: NSURL(string: strURL)! as URL,
                                                cachePolicy: .useProtocolCachePolicy,
                                            timeoutInterval: 100.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers

        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            DispatchQueue.main.async {
                self.removeLoader()
                if (error != nil) {
                    print(error.debugDescription)
                } else {
                    
                    guard data != nil else {
                        print("no data found: \(String(describing: error))")
                                    return
                    }

                    do {
                        if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
//                            print("Response: \(json)")                            
                            self.parseAndShowResponse(json)
                        }
                    } catch let parseError {
                        print(parseError)// Log the error thrown by `JSONObjectWithData`
                        let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        self.showAlertMessage(jsonStr! as String)
                    }
                }
            }
        })
        dataTask.resume()
    }
    
    func parseAndShowResponse(_ result:NSDictionary){
        
        UserDefaults.standard.setValue(result, forKey: "forecast")
        
        if((result["list"] as? NSObject) != nil  && ((result["list"] as? NSObject) is NSNull) == false){
            self.arrItems = result["list"] as! [[String:Any]]
            
            self.arrWeathers.removeAll()
            for dictW in self.arrItems {
                let objWeather:WeatherObject = WeatherObject.init(dictItem: dictW as NSDictionary)
                self.arrWeathers.append(objWeather)
            }
            self.filterKeys()
        } else {
            self.showAlertMessage(title:appDelegate.strLocality, message:result["message"] as? String ?? "No record found")
        }
    }
    
    func filterKeys() {
        self.tblWeather.dataSource = nil
        self.arrKeys.removeAll()
        self.weatherData.removeAll()
        
        for objW in self.arrWeathers {
            
            if(self.arrKeys.contains(objW.strDay) == false) {
                self.arrKeys.append(objW.strDay)
            }
            
            if (self.weatherData[objW.strDay] != nil) {
                self.weatherData[objW.strDay]!.append(objW)
            } else {
                self.weatherData[objW.strDay] = [objW]
            }
        }
        self.tblWeather.dataSource = self
        self.tblWeather.reloadData()
    }
}
