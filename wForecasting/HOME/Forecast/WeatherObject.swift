//
//  WeatherObject.swift
//  wForecasting
//
//  Created by Dilip Saket on 18/09/21.
//

import UIKit

class WeatherObject: NSObject {

    var strID: String = ""
    var strName: String = ""
    
    var strTemp: String = ""
    var strTime: String = ""
    
    var dateTime: Date = Date()
    var strDidplayTime: String = ""
    var strDay: String = ""
    
    override init() {
        // Database.database().reference()
    }
    
    init(dictItem:NSDictionary?)
    {
        if(dictItem == nil) {
            return
        }
        
        let dictMain:[String:Any]?  = (dictItem?["main"] as? [String:Any])
        if(dictMain == nil) {
            return
        }
        
        let arrWeather:[[String:Any]]  = (dictItem?["weather"] as? [[String:Any]]) ?? [[:]]
        if(arrWeather.count == 0) {
            return
        }
        let dictWeather:[String:Any] = arrWeather.first!
        if((dictWeather["id"] as? NSObject) != nil && ((dictWeather["id"] as? NSObject) is NSNull) == false){
            let numID:NSNumber = dictWeather["id"] as! NSNumber
            strID       = String(numID.intValue)
        }
        
        if((dictWeather["main"] as? NSObject) != nil && ((dictWeather["main"] as? NSObject) is NSNull) == false){
            strName = dictWeather["main"] as! String
        }
        if((dictMain!["temp"] as? NSObject) != nil && ((dictMain!["temp"] as? NSObject) is NSNull) == false){
            
            let temp:NSNumber = dictMain!["temp"] as! NSNumber
            strTemp       = String(Int(temp.doubleValue-273.15))
        }
        if((dictItem!["dt_txt"] as? NSObject) != nil && ((dictItem!["dt_txt"] as? NSObject) is NSNull) == false){
            strTime = dictItem!["dt_txt"] as! String
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateTime = dateFormatter.date(from: strTime) ?? Date()

            dateFormatter.dateFormat = "hh:mm a"
            strDidplayTime = dateFormatter.string(from: dateTime)
            
            let calender = NSCalendar.current
            let day3 = Calendar.current.date(byAdding: Calendar.Component.day, value: 2, to: Date())!
            let day4 = Calendar.current.date(byAdding: Calendar.Component.day, value: 3, to: Date())!
            let day5 = Calendar.current.date(byAdding: Calendar.Component.day, value: 4, to: Date())!
            let day6 = Calendar.current.date(byAdding: Calendar.Component.day, value: 5, to: Date())!
            
            if(calender.isDateInToday(dateTime) == true) {
                strDay = "Today"
            } else if(calender.isDateInTomorrow(dateTime) == true) {
                strDay = "Tomorrow"
            } else if(calender.isDate(dateTime, inSameDayAs: day3) == true) {
                dateFormatter.dateFormat = "EEEE"
                strDay = dateFormatter.string(from: dateTime)
            } else if(calender.isDate(dateTime, inSameDayAs: day4) == true) {
                dateFormatter.dateFormat = "EEEE"
                strDay = dateFormatter.string(from: dateTime)
            } else if(calender.isDate(dateTime, inSameDayAs: day5) == true) {
                dateFormatter.dateFormat = "EEEE"
                strDay = dateFormatter.string(from: dateTime)
            } else if(calender.isDate(dateTime, inSameDayAs: day6) == true) {
                dateFormatter.dateFormat = "EEEE"
                strDay = dateFormatter.string(from: dateTime)
            }
        }
    }
    
}
