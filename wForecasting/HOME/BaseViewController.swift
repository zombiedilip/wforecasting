//
//  BaseViewController.swift
//  wForecasting
//
//  Created by Dilip Saket on 18/09/21.
//

import UIKit
import SystemConfiguration

class BaseViewController: UIViewController {

    let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate    
    var loader: UIAlertController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK:- Base Methods
    func showAlertMessage(_ strMessage: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            
            let alert = UIAlertController(title: "", message: strMessage, preferredStyle: .alert)
            let yesButton = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Handle your yes please button action here
            })
            alert.addAction(yesButton)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showAlertMessage(title: String, message: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let yesButton = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Handle your yes please button action here
            })
            alert.addAction(yesButton)
            self.present(alert, animated: true, completion: nil)
        }
    }
    func addLoader()
    {
        if(loader == nil) {
            loader = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
            
            loader.view.tintColor = UIColor.black
            let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x:10, y:5, width:50, height:50)) as UIActivityIndicatorView
            loadingIndicator.hidesWhenStopped = true
//            loadingIndicator.style = UIActivityIndicatorView.Style.UIActivityIndicatorView.Style.medium
            loadingIndicator.startAnimating();
            
            loader.view.addSubview(loadingIndicator)
            present(loader, animated: true, completion: nil)
        }
    }
    
    func removeLoader()
    {
        if(loader != nil) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                if(self.loader != nil) {
                    self.loader.dismiss(animated: true, completion: nil)
                    self.loader = nil
                }
            }
        }
    }
    
    //MARK:- Reachability
    public static func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                
                SCNetworkReachabilityCreateWithAddress(nil, $0)
                
            }
            
        }) else {
            
            return false
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}
